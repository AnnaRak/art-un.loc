<?php require_once 'DB_connection.php';

function Input()
{
    if (isset($_POST['submit'])) {
        global $connect;
        $firstName = $_POST['first_name'];
        $lastName = $_POST['last_name'];

        $firstName = mysqli_real_escape_string($connect, $firstName);
        $lastName = mysqli_real_escape_string($connect, $lastName);

        $query = "INSERT INTO visitors(first_name, last_name) ";
        $query .= "VALUES ('$firstName', '$lastName') ";

        $res = mysqli_query($connect, $query);
        if (!$res) {
            die("failed");
        } else {
            header('location:/HW_mysql/showing_data.php');
        }
    }
}

function TaskOne()
{
    global $connect;
    $query = "SELECT interest, CONCAT(first_name, ' ', last_name) as StudentName
            FROM `camp_groups` 
            JOIN visitors ON camp_groups.visitor_id = visitors.id
            ORDER BY camp_groups.title";
    global $result;
    $result = mysqli_query($connect, $query);
    while ($row = mysqli_fetch_assoc($result))
        print_r($row);

}

function TaskTwo()
{
    $input = 11;
    global $connect;
    $query = "SELECT interest, CONCAT(first_name, ' ', last_name) as StudentName
         FROM `camp_groups`
         JOIN visitors ON camp_groups.visitor_id = visitors.id
         WHERE month_of_visit > $input;";
    $result = mysqli_query($connect, $query);
    while ($row = mysqli_fetch_assoc($result))
        print_r($row);
}

function TaskThree()
{
    $input = 2020;
    global $connect;
    $query = "SELECT interest, CONCAT(first_name, ' ', last_name) as StudentName
         FROM `camp_groups`
         JOIN visitors ON camp_groups.visitor_id = visitors.id
         WHERE year_of_visit = $input;";
    $result = mysqli_query($connect, $query);
    while ($row = mysqli_fetch_assoc($result))
        print_r($row);
}

function TaskFour()
{
    global $connect;
    $query = "SELECT interest, ROUND(AVG(visitor_id), 0) AS AverageStudents
            FROM `camp_groups` 
            WHERE year_of_visit BETWEEN '2017' AND '2020'
            GROUP BY interest; ";
    $result = mysqli_query($connect, $query);
    while ($row = mysqli_fetch_assoc($result))
        print_r($row);
}
