-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 24 2020 г., 19:41
-- Версия сервера: 5.7.23
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `artelogic`
--

-- --------------------------------------------------------

--
-- Структура таблицы `camp_groups`
--

CREATE TABLE `camp_groups` (
  `id` int(11) NOT NULL,
  `visitor_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `year_of_visit` year(4) NOT NULL,
  `month_of_visit` tinyint(2) NOT NULL,
  `interest` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `camp_groups`
--

INSERT INTO `camp_groups` (`id`, `visitor_id`, `title`, `year_of_visit`, `month_of_visit`, `interest`) VALUES
(1, 3, 'First group', 2017, 8, 'math'),
(2, 9, 'Second group', 2017, 9, 'literature'),
(5, 2, 'Third group', 2018, 6, 'science'),
(6, 11, 'Fourth group', 2018, 9, 'foreign_languages'),
(7, 8, 'Fifth group', 2019, 5, 'chemistry'),
(8, 7, 'Six group', 2020, 7, 'cybernetics'),
(9, 5, 'Seventh group', 2020, 11, 'math'),
(10, 10, 'Eight group', 2020, 12, 'physics'),
(11, 2, 'First group', 2017, 8, 'math'),
(12, 8, 'Third group', 2018, 6, 'science'),
(13, 1, 'Seventh group', 2020, 11, 'math'),
(14, 1, 'Eight group', 2020, 12, 'physics'),
(15, 1, 'Six group', 2020, 7, 'cybernetics'),
(16, 10, 'Seventh group', 2020, 11, 'math'),
(17, 11, 'Second group', 2017, 9, 'literature'),
(18, 5, 'Six group', 2020, 7, 'cybernetics'),
(19, 10, 'Six group', 2020, 7, 'cybernetics');

-- --------------------------------------------------------

--
-- Структура таблицы `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `visitors`
--

INSERT INTO `visitors` (`id`, `first_name`, `last_name`) VALUES
(1, 'Chris', 'Matte'),
(2, 'Ivan', 'Clark'),
(3, 'Alison', 'Metwes'),
(4, 'Josh', 'Boem'),
(5, 'Susie', 'Que'),
(6, 'Amanda', 'Sims'),
(7, 'James', 'Delfino'),
(8, 'Mike', 'Pitterson'),
(9, 'Christin', 'Adams'),
(10, 'Jenifer', 'Garner'),
(11, 'Gigi', 'Hadeet'),
(12, 'Din', 'Vinchester');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `camp_groups`
--
ALTER TABLE `camp_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visitors_id` (`visitor_id`),
  ADD KEY `title` (`title`),
  ADD KEY `year_of_visit` (`year_of_visit`),
  ADD KEY `month_of_visit` (`month_of_visit`),
  ADD KEY `interest` (`interest`);

--
-- Индексы таблицы `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `first_name` (`first_name`),
  ADD KEY `last_name` (`last_name`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `camp_groups`
--
ALTER TABLE `camp_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `camp_groups`
--
ALTER TABLE `camp_groups`
  ADD CONSTRAINT `camp_groups_ibfk_1` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
